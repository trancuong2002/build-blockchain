import { createTheme } from "@mui/material";

export const ThemeMdx = createTheme({
  components: {
    MuiAccordion: {
      styleOverrides: {
        // Name of the slot
        root: {
          color: "#FFFFFF",
          backgroundColor: "transparent",
          fontSize: "1rem",
          outline: "none",
          boxShadow: "none",
          padding: "0 0",
        },
      },
    },
    MuiSvgIcon: {
      styleOverrides: {
        root: {
          color: "#FFFFFF !important",
        },
      },
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          padding: "0 0 !important",
        },
      },
    },
  },
});
