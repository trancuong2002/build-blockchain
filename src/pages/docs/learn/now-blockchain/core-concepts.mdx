---
title: About Core Concepts
description: About Core Concepts.
imageTitle: About Core Concepts
path: /docs/core-concepts
---

import Image from "next/image";
import { Callout } from "nextra/components";

# Core Concepts

## UTxO Model

Now Blockchain uses the UTxO (Unspent Transaction Output) Model, like Bitcoin. A Transaction Output is a specific amount of Now Blockchain, identifiable through a unique ID called a Coin ID. Each Transaction Output can be considered analogous to a physical coin in that they can represent different amounts of currency and must be spent in whole.

The Now Blockchain blockchain keeps track of the UTxO set on the network and who can spend them. The UTxO set circulating in the network fluctuates as users spend coins and create new ones as outputs from transactions. The sum of the value of all the UTxOs in the network will equal at most 1 billion Now Blockchain. One or more UTxOs are used as inputs into transactions and one or more new UTxOs will be created as outputs.

The example below shows a transaction of 30 Now Blockchain from Bob to Alice. A UTxO worth 50 Now Blockchain is used as an input and two new UTxOs are created as outputs - one worth 30 Now Blockchain which is sent to the recipient and one of 20 Now Blockchain which is returned to the sender as change. Just like change is received when physical coins are spent.

<Image
  alt="Banner"
  src={"/assets/images/docs/learn/learn2.png"}
  width={751 * 1.5}
  height={439 * 1.5}
  className="nx-mt-4"
/>

Now Blockchain is the blockchain layer for value transfer. All transactions are processed by all nodes on the network. It is flood-fill. It uses the peer-to-peer network as its backbone for communication between nodes.

The Now Blockchain blockchain is where all on-chain transactions are processed. Every node in the network collectively comes to consensus on the state of the blockchain so all transactions are accounted for. Users initiate their transacting relationships on Layer 1, prior to moving off-chain to use Layer 2 for faster and cheaper transactions. As the trust layer of the protocol, Layer 1 is also used for settling any disputes between users on Layer 2.

## Transaction Proof-of-Work (TxPoW)

Now Blockchain requires users to provide work, in the form of hashing, to ‘mine’ their own transactions, this is TxPoW.

Once a user has contributed a small amount of work (~1 second of hashing) they have contributed enough for their transaction to be sent around the network.

<Callout type="warning" emoji="⚠️">
  <strong>NOTE</strong> <br />
  <strong>There are no financial rewards for mining your transactions</strong>
  e.g. block rewards or collection of fees, the reward is simply being able to
  broadcast a (valid) transaction to the network.
</Callout>

Transactions are held in [TxPoW units](/docs/learn/now-blockchain/txpowunits), which are propagated around the network and may or may not become blocks. TxPoW units contain a user’s transaction and the hashes of other unconfirmed transactions known to the user’s node. TxPoW units become blocks if they, by chance, meet the level of difficulty required to become a block.

<span>
  <strong className="pt-5">
    This ensures that all users can contribute to the construction of the chain
  </strong>
  <span>
    and is in contrast to Bitcoin where users rely on other dedicated 'miners'
    with specific hardware to provide PoW and propagate their transactions on
    their behalf.
  </span>
</span>

## The Burn

The Burn is a small cost (fee) which is incurred when sending transactions on the Now Blockchain network during times of high demand. This cost, denominated in Now Blockchain, is ‘burned’ i.e. removed from Now Blockchain's hard-capped supply, making Now Blockchain a deflationary currency as the overall circulating supply slowly decreases over time.

The burn serves multiple purposes:

- A strong incentive to propagate and process a transaction: All users in the network will benefit from the decrease in supply as coins that are left become more scarce and therefore more valuable.
- A method for ordering transactions and regulating on-chain traffic: Similar to the fee model on other blockchains, the burn serves as a selection method for deciding which unconfirmed transactions will be added to a block. The higher the burn amount in a transaction, the more likely it is that a transaction will be added to a block.
- A mechanism for spam prevention by making Denial-of-Service (DoS) attacks expensive: The burn may be high during periods of heavy traffic or spam and, as it rises, traffic will decrease, self-regulating the system. The burn can be nil or very low when traffic is at manageable levels as the total amount is not important, only the relative burn amount in comparison to other transactions.

While some blockchains have implemented burns that require a central entity to actively buy back and burn coins or tokens (for example by sending them to an inaccessible address, effectively destroying them); others have a burn directly integrated into the protocol.

In Now Blockchain, the ability to burn coins is directly coded into the protocol, meaning it does not rely on any entity, but rather is enforced by the network as a function of demand for block space.

## MMR (Merkle Mountain Range) Database

To ensure that all users of the network can contribute to the construction of the chain, the chain needs to be small enough to run on a mobile device. This would not be possible if the entire history of the chain was required as this would be too much of an overhead for a mobile device. Therefore the blockchain must be constantly reduced in size to meet this requirement. This is known as pruning.

The impact of pruning means that the full transaction history of the chain is not kept, therefore a storage mechanism is required to keep track of coins that were created in blocks that have since been pruned. This is the role of the MMR database.

Hence, users must keep track of their spent and unspent coins/TxOs (Transaction Outputs) independently of the chain. Each coin is stored as a leaf node in a tree structure (a Merkle hash-sum tree). Then, using a collection of nodes in this tree, a proof path can be created from the coin to a peak of the tree, proving the existence of a coin even if the block that it was created in has been pruned.

All users only keep the parts of the MMR tree required to create the proofs for their own coins, which is a tiny amount of data compared to all the coins in the network. Users are also required to store the peaks and the root of the tree so that they can validate a Coin Proof that is presented to them by another user.

When a user wishes to spend their coins, they must provide the up-to-date, valid proof that it is unspent. Any other node in the network can verify this proof by calculating the peaks and root hash of the MMR tree from the proof and ensuring it matches their own values for the peak and root hash.
