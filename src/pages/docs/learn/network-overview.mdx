---
title: About Network Overview
description: About Network Overview.
imageTitle: About Network Overview
path: /docs/network-overview
---

import Image from "next/image";

# Network Overview

The Now Blockchain network consists of four distinct layers.

Each layer contributes to Now Blockchain being a truly decentralized, censorship resistant, peer-to-peer network for value and information exchange.

## Layer 1: Now Blockchain

<br />
<strong>Value transfer</strong>
<br />

<strong>On-chain</strong>

<Image
  alt="Banner"
  src={"/assets/images/docs/learn/learn1.png"}
  width={751 * 1.5}
  height={439 * 1.5}
  className="nx-mt-4"
/>

Now Blockchain is the blockchain layer for value transfer. All transactions are processed by all nodes on the network. It is flood-fill. It uses the peer-to-peer network as its backbone for communication between nodes.

The Now Blockchain blockchain is where all on-chain transactions are processed. Every node in the network collectively comes to consensus on the state of the blockchain so all transactions are accounted for. Users initiate their transacting relationships on Layer 1, prior to moving off-chain to use Layer 2 for faster and cheaper transactions. As the trust layer of the protocol, Layer 1 is also used for settling any disputes between users on Layer 2.
