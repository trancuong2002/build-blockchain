// import "../custom.css";
import { Analytics } from "@vercel/analytics/react";
import "./styles.css";

export default function Nextra({ Component, pageProps }) {
  return (
    <>
      <Component {...pageProps} />
      <Analytics />
    </>
  );
}
