import img from "@/assets/images/home/bannerimg1.png";
import img2 from "@/assets/images/home/bannerimg2.png";
import bg from "@/assets/images/home/bannerbg1.png";
export const BANNER_DUMMY = [
  {
    img: img,
    bg: bg,
    title: "Powerful for developers. \n Fast for everyone.",
    des: "Bring blockchain to the people. Now Blockchain supports experiences for power users, new consumers, and everyone in between.",
    button1: "START BUILDING",
    button2: "READ DOCS",
  },
  {
    bg: bg,
    img: img2,
    title: "Marketing is The Best Way to Grow Your Business",
    button1: "START BUILDING",
    button2: "READ DOCS",
    des: "Bring blockchain to the people. Now Blockchain supports experiences for power users, new consumers, and everyone in between.",
  },
  {
    bg: bg,
    img: img,
    title: "Marketing is The Best Way to Grow Your Business",
    button1: "START BUILDING",
    button2: "READ DOCS",
    des: "Bring blockchain to the people. Now Blockchain supports experiences for power users, new consumers, and everyone in between.",
  },
];

export const BuildMenu = [
  {
    title: "Payments",
    url: "/",
  },
  {
    title: "Gaming",
    url: "/",
  },
  {
    title: "NFTs",
    url: "/",
  },
  {
    title: "Depi",
    url: "/",
  },
  {
    title: "Daos",
    url: "/",
  },
];
