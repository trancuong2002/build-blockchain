export const CardData = [
  {
    id: "1",
    title: "Fast",
    color: "#FF0000",
    des: "Don’t keep your users waiting. Now Blockchain has block times of 400 milliseconds — and as hardware gets faster, so will the network.",
    number: 3747,
    footer: "TRANSACTIONS PER SECOND",
  },
  {
    id: "2",
    title: "Decentralized",
    color: "#14FF00",
    des: "The Now Blockchain network is validated by thousands of nodes that operate independently of each other, ensuring your data remains secure and censorship resistant.",
    number: 1619,
    footer: "VALIDATOR NODES",
  },
  {
    id: "3",
    title: "Scalable",
    color: "#FFC700",
    des: "Get big, quick. Now Blockchain is made to handle thousands of transactions per second, and fees for both developers and users remain less than $0.0025.",
    number: 27093009853000,
    footer: "TRANSACTIONS PER SECOND",
  },
  {
    id: "4",
    title: "Energy Efficient",
    color: "#00F0FF",
    des: "Now blockchain's proof of stake network and other innovations minimize its impact on the environment. Each Now Blockchain transaction uses about the same energy as a few Google searches.",
    number: 0,
    footer: "NET CARBON IMPACT",
  },
];
