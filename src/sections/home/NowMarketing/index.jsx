import ArrowRightIcon from "@/icons/Arrow/ArrowRight";
import Image from "next/image";
import { EffectCoverflow, Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import imgBuild from "@/assets/images/home/build.png";
import shopify from "@/assets/images/home/shopify.png";
import { ListBuild } from "../ListBuild";
export const NowMarketingSection = () => {
  return (
    <div className="py-10 md:py-16 !pt-0 container flex items-center sm:gap-8 gap-4 flex-col">
      <div className="w-full h-full bg-gradient-transparent-purple rounded-xl flex items-center justify-around md:flex-row lg:flex-row flex-col gap-5 sm:gap-10 2xl:gap-20 md:px-10 px-1 lg:px-10 py-5 z-10">
        <div className="flex justify-center w-1/3 md:w-1/3 lg:w-auto">
          <Image
            src={imgBuild}
            alt="img"
            width={500}
            height={500}
            className="ml-auto w-56 sm:w-auto"
          />
        </div>
        <div className="w-full md:w-2/3 lg:w-auto flex flex-col items-center md:items-start gap-5 2xl:gap-7 justify-between max-w-[800px]">
          <p className="2xl:text-4xl xl:text-3xl md:text-2xl sm:text-3xl text-2xl text-center md:text-left font-semibold">
            PAYMENTS
          </p>
          <p className="text-white text-lg md:px-0 px-3 md:text-left sm:text-center text-left">
            Now Blockchain Pay is now available to millions of businesses as an
            approved app integration on Shopify. Now Blockchain Pay is built for
            immediate USDC transactions, fees that are fractions of a penny, and
            a net-zero environmental impact.
          </p>
          <button className="block w-max cursor-pointer relative font-bold duration-300 text-white px-5 py-2.5 md:px-7 md:py-3 text-sm md:text-base hover:-translate-y-0.5 bg-gradient-dark-pink rounded-full">
            Read More
          </button>
        </div>
      </div>
    </div>
  );
};
