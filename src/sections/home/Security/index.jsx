import Image from "next/image";
import security from "@/assets/images/home/security.png";
import { Title, Underline } from "../style";
export const Security = () => {
  return (
    <div className="py-10 md:py-16 w-full container flex flex-col md:flex-row items-start sm:gap-10 lg:gap-60 md:gap-70">
      <div
        data-aos="fade-up"
        className="lg:pt-[80px] pt-[10px] md:w-2/4 w-full lg:w-2/4 relative"
      >
        <Title>Join a community of millions.</Title>
        <Underline></Underline>
      </div>
      <div
        data-aos="fade-up"
        className="flex flex-col w-full items-start justify-start gap-3 md:gap-8 "
      >
        <div className="flex flex-col justify-start">
          <p className="lg:text-[50px] xl:text-[100px] text-[40px] font-extrabold bg-gradient-to-r from-[#F40074] to-[#fff] bg-clip-text text-transparent">
            11.5M+
          </p>
          <span className="text-white text-xl font-normal">
            ACTIVE ACCOUNTS
          </span>
        </div>
        <div className="flex flex-col justify-start">
          <p className="lg:text-[50px] xl:text-[100px] text-[40px] font-extrabold bg-gradient-to-r from-[#F40000] to-[#fff] bg-clip-text text-transparent">
            20.5M
          </p>
          <span className="text-white text-xl font-normal">NFTS MINTED</span>
        </div>
        <div className="flex flex-col justify-start">
          <p className="lg:text-[50px] xl:text-[100px] text-[40px] font-extrabold bg-gradient-to-r from-[#0036F4] to-[#fff] bg-clip-text text-transparent">
            $0.00025
          </p>
          <span className="text-white text-xl font-normal">
            AVERAGE COST PER TRANSACTION
          </span>
        </div>
      </div>
    </div>
  );
};
