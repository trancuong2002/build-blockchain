import { ContentLeftContainer } from "./style";

export default function ContentLeft() {
  return (
    <ContentLeftContainer>
      <h1 className="text-lg font-semibold ">
        Welcome to No Bad Questions, a series that helps answer some basic
        questions about blockchain, crypto, and web3.
      </h1>
      <p>
        For the last several years, you’ve probably heard quite a bit about the
        blockchain… but it’s very easy to get lost. It can sound a lot like
        people are speaking another language!
      </p>
      <p>
        If you’re here, welcome — you’re taking the first step towards learning.
        This guide will help you learn some of the basic blockchain concepts so
        you can start participating in the future of the internet.
      </p>
      <div>
        <p>What exactly IS the blockchain?</p>
        <p>
          At its core, the blockchain is a ledger — a series of records and data
          points, like a spreadsheet or old accounting book.
        </p>
      </div>
      <p>
        Unlike spreadsheets or old books, though, the blockchain
        is decentralized — instead of it being stored in one place, it’s stored
        across different computers across a network and around the world.
        Essentially, there’s no single book that can be easily lost or updated.
        Instead, the data is verified by checking the blocks (or bundles of
        transactions) across different validator nodes.
      </p>
      <div>
        <p>How does that relate to cryptocurrency?</p>
        <p>
          Well, let’s start with some definitions first. Cryptography is the art
          of keeping things secure. You’ve probably encountered this before,
          through encrypted data and passwords.
        </p>
      </div>
      <p>
        Cryptocurrency, then, is a type of currency that is based on
        cryptography. Rather than the ledger being centralized, transactions are
        decentralized — there’s no single authority validating everything — and
        as secure as other encrypted data. The translation? It’s a very safe way
        of storing data and value with no choke points or central points of
        failure and which is very difficult to manipulate.
      </p>
      <div>
        <p>
          So what exactly is a coin? What is a token? What’s the difference
          between coins and tokens?
        </p>
        <p>
          All blockchain protocols have coins that represent value and can be
          traded. On Now Blockchain, this coin is called SOL.
        </p>
      </div>
      <div>
        <p>Okay, wait. What is a smart contract?</p>
        <p>
          A smart contract is a contract that is written into the code of a
          blockchain and executed automatically when terms are met. It’s
          something that Now Blockchain and certain other protocols can do, but
          some other blockchains — most notably Bitcoin — cannot.
        </p>
      </div>
      <p>
        Think of a regular contract like a housing lease agreement. You pay
        rent; in exchange, you have a place to live. If you no longer have a
        place to live, you no longer pay rent; if you don’t pay rent, you’ll be
        kicked out. In a regular contract, the execution depends on the humans.
        A tenant fulfills their obligations of the lease and monitors that a
        landlord fulfills theirs, and vice versa.
      </p>
      <p>
        With a smart contract, however, that execution is hardcoded in software,
        and software determines when conditions are met. That allows people to
        build some really cool functionality and dApps.
      </p>
      <div>
        <p>DApps? What are dApps?</p>
        <p>
          A dApp is a decentralized application, or app that uses smart
          contracts. Any crypto program — like a marketplace, blockchain game,
          or tool used for DAO governance — is a dApp.
        </p>
        <p>
          DApps are the tools you use to do things on the blockchain, whether
          that’s buying some art, voting in your decentralized organization,
          paying without intermediaries, or participating in decentralized
          finance.
        </p>
      </div>
      <div>
        <p>And what is web3?</p>
        <p>
          Web3 is the next iteration of the internet. Each iteration of the
          internet has been based on specific ideas: If web 1.0 was static
          websites and hyperlinking (like AOL or GeoCities) and web2 was social
          media (like Twitter, Facebook, and so on), web3 is built around the
          idea of decentralization.
        </p>
      </div>
      <p>
        Instead of an internet controlled by a handful of powerful, large
        platforms, web3 is built around the idea of a diffuse internet that
        anyone can support and control. Think of it as a return to early
        internet values — web3 is an era that removes gatekeepers and
        centralized “news feed” silliness.
      </p>
      <p>
        It’s a world that can be built on the blockchain, using smart contracts.
      </p>
      <p>
        Oooookay, so I think I’m starting to get this. So what exactly is the
        advantage of all this decentralization?
      </p>
      <p>When the network isn’t controlled by a single entity, that means…</p>
      <ul>
        <li>
          <p>
            People who traditionally haven’t had access to banking have the
            ability to participate in the financial system.
          </p>
        </li>
        <li>
          <p>
            You can pay for your favorite coffee without any fees or
            intermediaries.
          </p>
        </li>
        <li>
          <p>
            Networks can’t be compromised or shut down due to one large
            controlling entity, like a nation state or powerful platform.
          </p>
        </li>
        <li>
          <p>
            Users themselves can easily have a stake or buy into the platforms
            they are using, voting on the future of a video game metaverse.
          </p>
        </li>
        <li>
          <p>And much more.</p>
        </li>
      </ul>
      <p>
        That’s the really exciting part of all this. It’s all extremely early in
        the web3 revolution, and you can watch it all happen and be a part of it
        — because web3 is all about community and participation. All these use
        cases are being built right now by developers around the world and you
        can help out by joining in.
      </p>
      <div>
        <p>Why Now Blockchain?</p>
        <p>
          This is the cool part — Now Blockchain is engineered for widespread,
          mainstream use by being energy efficient, lightning fast, and
          extremely inexpensive.
        </p>
      </div>
      <p>
        Many of the core Now Blockchain builders, like co-founder Anatoly
        Yakavenko, have a background in building cell phone networks. That means
        that they are singularly focused on building for scalability (the
        ability to grow) and efficiency (the ability to get the most information
        across with the least amount of resources). They believe that in order
        for people to build the projects that will get the public using
        blockchain technology, you need to make it as easy and painless as
        possible for people to experiment and use the technology as possible.
      </p>
      <p>
        Some of the current leading blockchain technologies use energy-hogging,
        time-consuming mining — or solving very complex calculations — to
        validate security, and have fees that can range into the hundreds of
        dollars per transaction. Now Blockchain uses what’s called proof of
        stake to validate information — there’s no mining involved — and a
        special innovation called proof of history on top of that that allows it
        to validate even quicker. That makes it extremely efficient, using
        energy at the same scale as a few Google searches and significantly less
        energy than other regular household uses like running your refrigerator.
        Transaction fees, which are used to maintain blockchain networks and
        have ballooned elsewhere, are a fraction of a cent on To Earn Now.
      </p>
      <p>
        All of that translates into projects and tools built on Now Blockchain
        that can be as frictionless and easy to use as the rest of the internet,
        for both developers and users.
      </p>
      <p>Want to learn more? Keep reading!</p>
    </ContentLeftContainer>
  );
}
