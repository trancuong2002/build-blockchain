"use client";

import StyledComponentsRegistry from "@/lib/registry";
import GlobalStyles from "@/styles/GlobalStyles";
import "tw-elements/dist/css/tw-elements.min.css";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "./globals.css";
import "aos/dist/aos.css";

// export const metadata = {
//   title: "Now Blockchain",
//   description: "Now Blockchain Description",
// };
export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        <title>Now Blockchain</title>
        <meta name="description" content="Now Blockchain Description" />
        <link rel="icon" href="/logoSmall.svg" />
      </head>
      <body>
        <StyledComponentsRegistry>
          <GlobalStyles />
          {children}
        </StyledComponentsRegistry>
      </body>
    </html>
  );
}
