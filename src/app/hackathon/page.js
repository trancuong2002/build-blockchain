"use client";
import { MainLayout } from "@/layout/MainLayout";
import AccountCleanUp from "@/sections/AccountCleanUp";
import Hackathon from "@/sections/Hackathon";

export default function Page() {
  return (
    <MainLayout>
      <Hackathon />
    </MainLayout>
  );
}
