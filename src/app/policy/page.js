"use client";
import { MainLayout } from "@/layout/MainLayout";
import Policy from "@/sections/Policy";
export default function Page() {
  return (
    <MainLayout>
      <Policy />
    </MainLayout>
  );
}
