"use client";
import { MainLayout } from "@/layout/MainLayout";
import { Gaming } from "@/sections/Gaming";

export default function Page() {
  return (
    <MainLayout>
      <Gaming />
    </MainLayout>
  );
}
