"use client";
import { MainLayout } from "@/layout/MainLayout";
import Token from "@/sections/Token";
export default function Page() {
  return (
    <MainLayout>
      <Token />
    </MainLayout>
  );
}
