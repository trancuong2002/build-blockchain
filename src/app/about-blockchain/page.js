"use client";
import { MainLayout } from "@/layout/MainLayout";
import { AboutBlockchain } from "@/sections/AboutBlockchain";
import AccountCleanUp from "@/sections/AccountCleanUp";

export default function Page() {
  return (
    <MainLayout>
      <div>
        <AboutBlockchain />
      </div>
    </MainLayout>
  );
}
