"use client";
import { MainLayout } from "@/layout/MainLayout";
import AccountCleanUp from "@/sections/AccountCleanUp";
import { Environment } from "@/sections/Environment";

export default function Page() {
  return (
    <MainLayout>
      <div>
        <Environment />
      </div>
    </MainLayout>
  );
}
