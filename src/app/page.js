"use client";
import { MainLayout } from "@/layout/MainLayout";
import HomeContainer from "@/sections/home";

export default function Home() {
  return (
    <MainLayout>
      <HomeContainer />
    </MainLayout>
  );
}
