"use client";
import { MainLayout } from "@/layout/MainLayout";
import AccountCleanUp from "@/sections/AccountCleanUp";
import { Developer } from "@/sections/Develop";
import { Environment } from "@/sections/Environment";

export default function Page() {
  return (
    <MainLayout>
      <Developer />
    </MainLayout>
  );
}
