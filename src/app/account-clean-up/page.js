"use client";
import { MainLayout } from "@/layout/MainLayout";
import AccountCleanUp from "@/sections/AccountCleanUp";

export default function Page() {
  return (
    <MainLayout>
      <div
        className="bg-cover bg-center bg-no-repeat relative"
        style={{
          backgroundImage: 'url("../../assets/images/layer.png")',
        }}
      >
        <AccountCleanUp />
      </div>
    </MainLayout>
  );
}
