import Footer from "@/components/Footer";
import Header from "@/components/Header";
import AOS from "aos";
import { useEffect } from "react";

export const MainLayout = (props) => {
  useEffect(() => {
    AOS.init();
  }, []);
  const { children, hasFooter = true, hasHeader = true, hasBrandForm } = props;
  return (
    <div>
      {hasHeader && <Header />}
      {children}
      {hasFooter && <Footer />}
    </div>
  );
};
