import { communities, navFooter } from "@/constants";
import { PATHS } from "@/routes";
import Image from "next/image";
import Link from "next/link";
import {
  BotContainer,
  CategoryContainer,
  FooterContainer,
  Icon,
  LeftSide,
  LogoWrapper,
  Name,
  RightSide,
  Top,
  TopContainer,
} from "./styles";
const Footer = ({ isDocs }) => {
  return (
    <footer
      className={`${
        !isDocs ? "bg-black-2" : "transparent"
      } relative text-primary-3 border-t-[1px] border-[#282829]`}
    >
      <TopContainer>
        <LogoWrapper>
          <Link href={PATHS.HOME}>
            <Image
              src="/iconFooter.png"
              width={300}
              height={60}
              alt={`Logo`}
              className="mx-auto max-w-64 sm:max-w-none"
            />
          </Link>
        </LogoWrapper>
        <Top>
          {navFooter?.map((category) => (
            <CategoryContainer key={category?.title}>
              <Name>{category?.title}</Name>
              {category?.child?.map((child, index) => (
                <Link
                  key={index}
                  href={child?.link}
                  target={`${index == 0 || index == 1 ? "_blank" : ""}`}
                  rel="noopener noreferrer"
                >
                  {child?.name}
                </Link>
              ))}
            </CategoryContainer>
          ))}
        </Top>
      </TopContainer>
      <BotContainer>
        <LeftSide>
          CopyRight © 2024 Now Blockchain. All rights reserved.
        </LeftSide>
        <RightSide>
          {communities?.map((child, index) => (
            <Icon key={index} target="_blank">
              {child?.icon}
            </Icon>
          ))}
        </RightSide>
      </BotContainer>
    </footer>
  );
};
export default Footer;
