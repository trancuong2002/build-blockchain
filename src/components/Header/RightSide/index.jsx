import { DivWrapper, Login, Register, RightSide } from "./styles";
import useAuth from "@/hooks/useAuth";
import { PATHS } from "@/routes";

const Header = (props) => {
  const { isAuth } = useAuth();
  return (
    <RightSide>
      {!isAuth && (
        <DivWrapper>
          <Register routeName={PATHS.REGISTER}>Register</Register>
          <Login routeName={PATHS.LOGIN}>Login</Login>
        </DivWrapper>
      )}
    </RightSide>
  );
};

export default Header;
